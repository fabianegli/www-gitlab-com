stages:

  # Stages are defined in the product categories handbook https://about.gitlab.com/handbook/product/categories/#hierarchy

  manage:
    display_name: "Manage"
    marketing: true
    image: "/images/solutions/solutions-manage.png"
    description: "Gain visibility and insight into how your business is performing."
    body: |
          GitLab helps teams manage and optimize their software delivery lifecycle with metrics and value stream insight in order to streamline and increase their delivery velocity.   Learn more about how GitLab helps to manage your end to end <a href="https://about.gitlab.com/vsm">value stream.</a>
    vision: /direction/manage
    roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Amanage&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
    established: 2016
    related:
      - "plan"
      - "create"
      - "verify"
      - "package"
      - "release"
      - "configure"
      - "monitor"
      - "secure"
      - "defend"
    dept: dev
    pm: Jeremy Watson
    pmm: John Jeremiah
    cm: Suri Patel
    backend_engineering_manager: Liam McAndrew
    frontend_engineering_manager: Dennis Tang (Interim)
    test_automation_engineers:
      - Sanad Liaquat
    ux: Chris Peressini
    tech_writer: Evan Read
    groups:
      control:
        name: Control
        categories:
          - audit_management
          - authentication_and_authorization
          - workflow_policies
      measure:
        name: Measure
        categories:
          - cycle_analytics
          - devops_score
          - code_analytics
      framework:
        name: Framework

  plan:
    display_name: "Plan"
    marketing: true
    image: "/images/solutions/solutions-plan.png"
    description: "Regardless of your process, GitLab provides powerful planning
      tools to keep everyone synchronized."
    body: |
      GitLab enables portfolio planning and management through epics, groups (programs) and milestones to organize and track progress.  Regardless of your methodology from Waterfall to DevOps, GitLab’s simple and flexible approach to planning meets the needs of small teams to large enterprises.

      GitLab helps teams organize, plan, align and track project work to ensure teams are working on the right things at the right time and maintain end to end visibility and traceability of issues throughout the delivery lifecycle from idea to production.
    vision: /direction/plan
    roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Aplan&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
    established: 2011
    related:
      - "create"
    dept: dev
    pm: Victor Wu
    pmm: John Jeremiah
    cm: Suri Patel
    backend_engineering_manager: Sean McGivern
    frontend_engineering_manager: André Luís
    test_automation_engineers:
      - Ramya Authappan
    ux: Pedro Moreira da Silva
    tech_writer: Mike Lewis
    groups:
      team_planning:
        name: Team Planning
        categories:
          - project_management
          - kanban_boards
          - time_tracking
      enterprise_planning:
        name: Enterprise Planning
        categories:
          - agile_portfolio_management
          - value_stream_management
      certify:
        name: Certify
        categories:
          - requirements_management
          - quality_management
          - service_desk

  create:
    display_name: "Create"
    marketing: true
    image: "/images/solutions/solutions-create.png"
    description: "Create, view, and manage code and project data through powerful branching tools."
    body: |
      GitLab helps teams design, develop and securely manage code and project data from a single distributed version control system to enable rapid iteration and delivery of business value.  GitLab repositories provide a scalable, single source of truth for collaborating on projects and code which enables teams to be productive without disrupting their workflows.
    vision: /direction/create
    roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Acreate&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
    established: 2011
    related:
      - "plan"
      - "verify"
    dept: dev
    pm: James Ramsay
    pmm: John Jeremiah
    cm: Suri Patel
    backend_engineering_manager: Douwe Maan
    frontend_engineering_manager: André Luís
    test_automation_engineers:
      - Mark Lapierre
      - Tomislav Nikić
    ux: Jeethu
    tech_writer: Marcia Ramos
    groups:
      review:
        name: Review
        categories:
          - code_review
          - design_management
      source_code:
        name: Source Code
        categories:
          - source_code_management
          - snippets
          - search
      editor:
        name: Editor
        categories:
          - web_ide
          - live_coding
          - wiki
      gitter:
        name: Gitter
        pm: James Ramsay
        pmm: John Jeremiah
        backend_engineering_manager: Tommy (interim)
        frontend_engineering_manager: Tim Z (Interim)
        ux: Dimitrie Hoekstra
        tech_writer: Axil
        categories:
          - gitter

  verify:
    display_name: "Verify"
    marketing: true
    image: "/images/solutions/solutions-verify.png"
    description: "Keep strict quality standards for production code with automatic testing and reporting."
    body: |
      GitLab helps delivery teams fully embrace continuous integration to automate the builds, integration and verification of their code.  GitLab’s industry leading CI capabilities enables automated testing, Static Analysis Security Testing, Dynamic Analysis Security testing and code quality analysis to provide fast feedback to developers and testers about the quality of their code.  With pipelines that enable concurrent testing and parallel execution, teams quickly get insight about every commit, allowing them to deliver higher quality code faster.
    vision: /direction/verify
    roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name[]=devops%3Averify&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
    established: 2012
    related:
      - "create"
    dept: cicd
    pm: "Brendan O'Leary"
    pmm: William Chia
    cm: Aricka Flowers
    backend_engineering_manager: Elliot Rushton
    frontend_engineering_manager: Tim Z (Interim)
    ux: Dimitrie Hoekstra
    tech_writer: Evan Read
    internal_customers:
      - Quality Department
      - UX Department
    groups:
      ci:
        name: "CI & Runner"
        categories:
          - continuous_integration
      testing:
        name: "Testing & Other"
        categories:
          - code_quality
          - performance_testing
          - system_testing
          - usability_testing
          - accessibility_testing
          - compatibility_testing

  package:
    display_name: "Package"
    marketing: true
    image: "/images/solutions/solutions-package.png"
    description: "Deploy quickly at massive scale with integrated Docker Container Registry"
    body: |
      GitLab helps teams package their applications and manage their containers with a secure and private container registry for their Docker images. The GitLab Container Registry isn't just a standalone registry; it's completely integrated with GitLab. You can now easily use your images for GitLab CI, create images specific for tags or branches and much more.

      Our container registry is fully integrated with Git repository management and comes out of the box with GitLab. This means our integrated Container Registry requires no additional installation. It allows for easy upload and download of images from GitLab CI.
    vision: /direction/package
    roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Apackage&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
    established: 2016
    dept: cicd
    pm: Joshua Lambert (interim)
    pmm: William Chia
    cm: Aricka Flowers
    backend_engineering_manager: Marin Jankovski (Interim)
    frontend_engineering_manager: Clement Ho
    ux: Hazel
    tech_writer: Axil
    internal_customers:
      - Distribution Team
    groups:
      package:
        name: Package
        categories:
          - container_registry
          - maven_repository
          - npm_registry
          - rubygem_registry
          - linux_package_registry
          - helm_chart_registry
          - dependency_proxy

  secure:
    display_name: "Secure"
    marketing: true
    image: "/images/solutions/solutions-secure.png"
    description: "Security capabilities, integrated into your development lifecycle."
    body: |
      GitLab provides Static Application Security Testing (SAST), Dynamic Application Security Testing (DAST), Container Scanning, and Dependency Scanning to help you deliver secure applications.
    vision: /direction/secure
    roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Asecure&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
    established: 2017
    related:
      - "create"
      - "defend"
    dept: secure
    pm: Fabio Busatto
    pmm: Cindy Blake
    cm: Erica Lindberg
    backend_engineering_manager: Philippe Lafoucrière
    frontend_engineering_manager: Lukas Eipert (Interim)
    ux: Andy
    tech_writer: Axil
    internal_customers:
      - Security Department
    groups:
      sast:
        name: SAST
        categories:
          - static_application_security_testing
          - secret_detection
      dast:
        name: DAST
        categories:
          - dynamic_application_security_testing
          - interactive_application_security_testing
          - fuzzing
      sca:
        name: Software Composition Analysis
        categories:
          - dependency_scanning
          - license_management
      container_scanning:
        name: Container Scanning
        categories:
          - container_scanning
      language-specific:
        name: Language-specific
      security-research:
        name: Security Research

  release:
    display_name: "Release"
    marketing: true
    image: "/images/solutions/solutions-release.png"
    description: "GitLab's integrated CD solution allows you to ship code with zero-touch, be it on one or one thousand servers."
    body: |
      GitLab helps automate the release and delivery of applications, shortening the delivery lifecycle, streamlining manual processes, and accelerating team velocity. With zero-touch Continuous Delivery (CD) built right into the pipeline, deployments can be automated to multiple environments like staging and production, and the system just knows what to do without being told - even for more advanced patterns like canary deployments. With feature flags, built-in auditing/traceability, on-demand environments, and GitLab pages for static content delivery, you'll be able to deliver faster and with more confidence than ever before.
    vision: /direction/release
    roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Arelease&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
    established: 2016
    related:
      - "configure"
    dept: cicd
    pm: Jason Lenny
    pmm: William Chia
    cm: Aricka Flowers
    backend_engineering_manager: Darby Frey
    frontend_engineering_manager: John Hampton
    ux: Dimitrie Hoekstra
    tech_writer: Marcia Ramos
    internal_customers:
      - Distribution Team
      - Gitter
      - Security Department
    groups:
      release_core:
        name: "Core Release"
        categories:
          - continuous_delivery
          - incremental_rollout
          - release_orchestration
          - release_governance
      release_support:
        name: "Supporting Capabilities"
        categories:
          - pages
          - review_apps
          - feature_flags

  configure:
    display_name: "Configure"
    marketing: true
    image: "/images/solutions/solutions-configure.png"
    description: "Configure your applications and infrastructure."
    body: "GitLab helps teams to configure and manage their application environments.  Strong integrations to Kubernetes simplifies the effort to define and configure the infrastructure needed to support your application.   Protect access to key infrastructure configuration details such as passwords and login information by using ‘secret variables’ to limit access to only authorized users and processes."
    vision: /direction/configure
    roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Aconfigure&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
    established: 2018
    related:
      - "configure"
    dept: ops
    pm: Daniel Gruesso
    pmm: William Chia
    cm: Aricka Flowers
    backend_engineering_manager: Dylan Griffith
    frontend_engineering_manager: Tim Z (Interim)
    test_automation_engineers:
      - Dan Davison
    ux: Taurie
    tech_writer: Evan Read
    internal_customers:
      - Quality Department
      - Site Availability Engineering
      - Site Reliability Engineering
    groups:
      auto_devops:
        name: "Auto DevOps & Kubernetes"
        categories:
          - auto_devops
          - kubernetes_configuration
          - chatops
          - runbook_configuration
          - chaos_engineering
          - cluster_cost_optimization
      serverless:
        name: "Serverless & PaaS"
        categories:
          - serverless
          - paas

  monitor:
    display_name: "Monitor"
    marketing: true
    image: "/images/solutions/solutions-monitor.png"
    description: "Automatically monitor metrics so you know how any change in code
      impacts your production environment."
    body: |
      You need feedback on what the effect of your release is in order to do
      release management. Get monitoring built-in, see the code change
      right next to the impact that it has so you can respond quicker and
      effectively. No need to babysit deployment to manually get feedback.
      Automatically detect buggy code and prevent it from affecting the
      majority of your users.
    vision: /direction/monitor
    roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Amonitor&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
    established: 2016
    related:
      - "release"
    dept: ops
    pm: Joshua Lambert
    pmm: William Chia
    cm: Aricka Flowers
    backend_engineering_manager: Seth Engelhard
    frontend_engineering_manager: Clement Ho
    ux: Amelia
    tech_writer: Axil
    internal_customers:
      - Infrastructure Department
    groups:
      apm:
        name: APM
        categories:
          - metrics
          - tracing
          - logging
      debugging_health:
        name: "Debugging and Health"
        categories:
          - error_tracking
          - cluster_monitoring
          - synthetic_monitoring
          - incident_management
          - status_page

  defend:
    display_name: "Defend"
    marketing: true
    description: "Defend your apps and infrastructure from security intrusions."
    body: |
      GitLab provides runtime application security, threat detection and management, data security, and application infrastructure security.
    vision: /direction/defend
    roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Adefend&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
    established: 2019
    related:
      - "release"
      - "secure"
      - "manage"
    dept: defend
    pm: Fabio Busatto
    pmm: Cindy Blake
    cm: Erica Lindberg
    backend_engineering_manager: Philippe Lafoucrière
    frontend_engineering_manager: Lukas Eipert (Interim)
    ux: Andy
    tech_writer: Axil
    internal_customers:
      - Security Department
    groups:
      runtime_application_security:
        name: Runtime Application Security
        categories:
          - runtime_application_self_protection
          - web_firewall
      threat_management:
        name: "Threat Management"
        categories:
          - threat_detection
          - behavior_analytics
          - vulnerability_management
      application_infrastructure_security:
        name: Application Infrastructure Security
        categories:
          - data_loss_prevention
          - storage_security
          - container_network_security

  growth:
    display_name: Growth
    dept: growth
    related:
      - manage
      - create
      - verify
      - package
      - configure
      - monitor
      - secure
      - defend
    established: 2019
    groups:
      acquisition:
        name: "Acquisition and Activation"
        pm: Tamas Szuromi
        engineering_manager: Liam McAndrew
        frontend_engineering_manager: Dennis Tang (Interim)
      smau:
        name: "Stage Cross Use (SMAU)"
        pm: Tamas Szuromi
        engineering_manager: Liam McAndrew
        frontend_engineering_manager: Dennis Tang (Interim)
      converstion:
        name: "Conversion and Upsell"
        pm: Tamas Szuromi
        engineering_manager: Liam McAndrew
        frontend_engineering_manager: Dennis Tang (Interim)
      retention:
        name: "Retention and Reactivation"
        pm: Tamas Szuromi
        engineering_manager: Liam McAndrew
        frontend_engineering_manager: Dennis Tang (Interim)

  enablement:
    display_name: Enablement
    dept: enablement
    related:
      - "create"
    established: 2012
    groups:
      distribution:
        name: Distribution
        image: "/images/solutions/solutions-distribution.png"
        description: "Add distribution description here"
        body: |
          GitLab helps distribution
        vision: /direction/distribution
        roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name[]=devops%3Adistribution&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
        pm: Joshua Lambert (interim)
        pmm: William Chia
        backend_engineering_manager: Marin Jankovski
        frontend_engineering_manager: Clement Ho
        ux: Dimitrie Hoekstra
        tech_writer: Axil
        categories:
          - omnibus
          - cloud_native_installation
        internal_customers:
          - Quality Department
          - Infrastructure Department
      fulfillment:
        name: Fulfillment
        pm: Jeremy Watson
        pmm: John Jeremiah
        cm: Suri Patel
        backend_engineering_manager: Liam McAndrew
        frontend_engineering_manager: Dennis Tang (Interim)
        ux: Chris Peressini
        tech_writer: Evan Read
        categories:
          - licensing
          - transactions
          - telemetry
      geo:
        name: "Geo"
        image: "/images/solutions/solutions-geo.png"
        description: "Add Geo description here"
        body: |
          GitLab makes Geo
        vision: /direction/geo
        roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name[]=devops%3Adistribution&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
        pm: Andreas Kämmerle
        pmm: John Jeremiah
        backend_engineering_manager: Rachel Nienaber
        frontend_engineering_manager: André Luís (Interim)
        ux: Dimitrie Hoekstra
        tech_writer: Evan Read
        categories:
          - geo
      gitaly:
        name: "Gitaly"
        image: "/images/solutions/solutions-gitaly.png"
        description: "Gitaly is a Git RPC service for handling all the git calls made by GitLab."
        body: |
          GitLab makes Gitaly
        vision: /direction/gitaly
        roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name[]=devops%3Adistribution&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
        related:
          - "create"
        pm: James Ramsay
        pmm: John Jeremiah
        backend_engineering_manager: Tommy (interim)
        frontend_engineering_manager: Tim Z (Interim)
        ux: Dimitrie Hoekstra
        tech_writer: Axil
        internal_customers:
          - Quality Department
          - Infrastructure Department
        categories:
          - gitaly
      memory:
        name: Memory
        description: "Responsible for optimizing GitLab application performance by reducing and optimizing memory resources required."
      ecosystem:
        name: Ecosystem
        description: "Supporting the success of third-party products integrating with GitLab."
        categories:
          - service_integrations
