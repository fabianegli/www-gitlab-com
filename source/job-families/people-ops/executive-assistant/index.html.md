---
layout: job_family_page
title: "Executive Assistant"
---

## Intermediate

### Responsibilities

* Executive assistants at GitLab toggle seamlessly between various systems including G-Suite, Slack, Expensify, Zoom and GitLab to accomplish an array of tasks, while staying focused on prioritization and escalating urgent issues
* We’re searching for a self-driven, collaborative, and agile team member who is experienced in managing multiple priorities, juggling various responsibilities, and anticipating
executives’ needs
* The ideal candidate will be exceptionally organized personally and enjoys organizing for others, has a deep love of logistics and thrives in a dynamic start-up environment.
* Support our Executive Leadership Team in EST - PST timezones
* Manage complex calendar including vetting, prioritizing and providing recommendations
* Total travel coordination including air and ground transportation, hotel reservations, security, visas and other travel documentation
* Maintain an efficient flow of information between all levels including internal and external contacts on a wide spectrum of plans and priorities
* Draft internal and external communications on behalf of executive
* File expense reports and track reimbursement status
* Manage projects, internal and external meetings and large-scale events from budget planning through logistical coordination
* May coordinate, track and schedule internal and external PR and communication events for manager and team
* Partner with PeopleOps, IT and Security to resolve any logistical issues
* Assist the full life cycle of recruiting including booking interviews, liaising between the hiring team and the candidate, and coordinating onboarding of new hires
* Maintains and tracks manager and teams project list and goals
* Attend team staff meetings, track agenda and assist Executive to keep team on course
* Provide general project support as needed including ad-hoc reporting
* Provide coverage for other E-Group EAs





### Requirements

* Minimum of 3 years of executive administration supporting more than one executive
* Successful history of managing the calendars, expenses, and travel of multiple executives
* Experience predicting, prioritizing, and assisting an executive’s workload
* Extensive technical skills in Google Suite, Zoom, Slack, and Expensify
* Must be proactive and able to deal with ambiguity, prioritize own work and resources, and juggle multiple tasks in a manner transparent to the team, and work independently to achieve results with a high degree of accuracy
* Exceptional communication and interpersonal skills and ability to interact autonomously with internal and external partners
* Maintain the confidentiality of highly sensitive material with tact and professionalism
* Demonstrated ability to adopt technical tools quickly (i.e. terminal, text editor)
* Possess an ability to multi-task and prioritize in a dynamic environment
* Superior attention to detail
* Event coordination and creative event planning experience
* Excellent written and verbal English communication skills
* Experience in a start-up environment preferred
* Experience working remotely preferred
* A desire to learn, have fun and work hard
* A passion for GitLab
* A sincere willingness to help out
* Able to work collaboratively with Administrative teammates across the org
* An orientation towards team success
- Successful completion of a [background check](/handbook/people-operations/code-of-conduct/#background-checks).

## Senior Executive Assistant 

### Responsibilities

* Support members of our Executive Leadership Group in PT, CT, and ET time-zones
* Own and proactively manage a complex, changing, high-volume calendar across multiple time-zones. Ensure Exec is prepped for every meeting.
* Make recommendations for the Executive in regard to their time management, prioritization, delegation and organization
* Seamlessly coordinate extensive domestic and international travel including air and ground transportation, hotel reservations, security, visas and other travel documentation. Accompanying the Executive when necessary.
* Plan events such as team off-sites, team building activities and company kick-offs as needed
* Assist the full life cycle of recruiting including booking interviews, liaising between the hiring team and the candidate, and coordinating onboarding of new hires
* Draft internal and external communications on behalf of Executive
* Compile receipts to submit timely and accurate expense reports on a monthly basis
* Attend team staff meetings, track agenda and assist Executive to keep team on course
* Provide coverage for other Sr. E-Group EA’s
* Run and lead special projects upon request
* Must be able to work flexible hours to support international business meetings
* Other duties as assigned in support of the business (adhoc tasks)

### Requirements

* Minimum 5 years supporting a C-Level Executive(s) as an executive business partner or Sr. EA at a quickly-scaling company
* Bachelor's’ Degree preferred. High school diploma or general education degree (GED) required
* Self-starter who can operate independently and move quickly from one task to another; creative problem solver, seeks "win-win" solutions; energized by challenges with superb attention to detail
* Extensive technical skills with Google Suite, Zoom, Slack, and Expensify among other tools
* Demonstrated Leadership mindset in prior roles—ability to influence culture/environment around them
* Proven leadership skills and demonstrates “one team” mindset; able to lead other EA’s and partner well across GitLab
* Experience with event planning & coordination to include support for large meetings, off-sites, and company events
* Detailed and goal-oriented planner; possesses the ability to appropriately prioritize business needs and handle multiple tasks in a fast-paced environment
* Approachable and effective communicator across various communication channels and with all levels of the organization
* Demonstrated ability to adopt technical tools quickly (i.e. terminal, text editor) 
* Experience in a start-up environment preferred
* Experience working remotely preferred
* A passion for GitLab
* A sincere willingness to help out


## Executive Assistant Manager 

### Responsibilites

* Support our Chief Executive Officer and Chief Financial Officer in PT time-zone with supplemental back-up support to our Executive Leadership Group in PT, CT, and ET time-zones. 
* Own and proactively manage a complex, changing, high-volume calendar across multiple time-zones. Ensure CEO and CFO are prepped for every meeting
* Make recommendations for the CEO and CFO with regard to their time management, prioritization, delegation and organization
* Leading, expanding, and mentoring the Administrative team by setting the strategy and prioritizing GitLab’s Objectives and Key Results (OKRs); hiring, training, and developing a world-class team
* Maintain the health of the EA organization, including hiring, on-boarding, training and development, and capacity planning
* Act as a liaison, problem solver, and facilitator as the first contact for the CEO
* Seamlessly coordinate extensive domestic and international travel including air and ground transportation, hotel reservations, security, visas and other travel documentation. Accompanying the CEO when necessary.
* Create and implement systems to elevate efficiency and effectiveness of administrative processes
* Run cadence of weekly staff meetings and other important meetings, including planning agenda, organization, and follow-up on action items, while infusing our company values throughout
* Plan events such as quarterly E-Group team off-sites, team building activities and company kick-offs as needed
* Serves as the CEO/CFO’s administrative liaison to the Board of Directors and manages Board activities, which include coordinating quarterly Board meetings and lunches and compiling, assembling, and distributing Board meeting materials
* Coordinates Executive, Finance, Governance & Nominating, and Audit Committee meetings, maintains confidential files and contact information; coordinates meetings, conferences, and committees both on and off site to support the CEO/CFO’s agenda
* Assist the full life cycle of recruiting including booking interviews, liaising between the hiring team and the candidate, and coordinating onboarding of new hires
* Draft internal and external communications on behalf of CEO and CFO
* Compile receipts to submit timely and accurate expense reports on a monthly basis
* Provide coverage for other Sr. E-Group EA’s
* Run and lead special projects upon request
* Must be able to work flexible hours to support international business meetings
* Other duties as assigned in support of the business (adhoc tasks)

### Requirements 
* Minimum 5 years supporting a C-Level Executive(s) as an executive business partner or Sr. EA at a quickly-scaling company
* Minimum 2 years experience managing a team
* Bachelor's’ Degree preferred. High school diploma or general education degree (GED) required
* Self-starter who can operate independently and move quickly from one task to another; creative problem solver, seeks "win-win" solutions; energized by challenges with superb attention to detail
* Extensive technical skills with Google Suite, Zoom, Slack, and Expensify among other tools
* Demonstrated Leadership mindset in prior roles—ability to influence culture/environment around them
* Proven leadership skills and demonstrates “one team” mindset; able to lead other EA’s and partner well across GitLab
* Experience with event planning & coordination to include support for large meetings, off-sites, and company events
* Detailed and goal-oriented planner; possesses the ability to appropriately prioritize business needs and handle multiple tasks in a fast-paced environment
* Approachable and effective communicator across various communication channels and with all levels of the organization
* Experience in a start-up environment preferred
* Experience working remotely preferred 
* A passion for GitLab
* A sincere willingness to help out



