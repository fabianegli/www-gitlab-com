---
layout: job_family_page
title: "Frontend Engineering Manager"
---

{: .text-center}
<br>

## Responsibilities

* Leading a team in the frontend department
* Coordination of the implementation of significant frontend features
* Works with other teams to identify priorities for releases
* Assigns frontend engineers direction issues
* Identifies hiring needs for frontend department
* Interviews candidates for Frontend engineers positions
* Continues to spend part of their time coding
* Ensures that the technical decisions and process set by the CTO are followed
* Does 1:1's with all reports every week
* Mentors and helps engineers to actively develop their skills
* Unblocks and coordinates development efforts
* Ensures quality implementation of design materials
* Review of merge requests made by Frontend engineers
* Delivers input on promotions, function changes, demotions and firings in consultation with the CEO, CTO, and VP of Engineering
* Defines best practices and coding standards for frontend group
* Helps to scale the team, tooling and our workflows

## Requirements for Candidates

* At least 1 year of experience leading, mentoring, and training teams of frontend engineers
* Experience working on a production-level JavaScript applications
* Self-motivated and strong organizational skills
* Strong understanding of CSS
* Expert knowledge of JavaScript
* Passionate about testing and design principles
* Basic knowledge of Vue.js is a plus but not a requirement
* Collaborate effectively with UX Designers, Developers, and Designers
* Be able to work with the rest of the community
* Knowledge of Ruby on Rails is a plus
* You share our [values](/handbook/values), and work in accordance with those values.

## Relevant links

- [Engineering Handbook](/handbook/engineering)
- [Engineering Workflow](/handbook/engineering/workflow)
