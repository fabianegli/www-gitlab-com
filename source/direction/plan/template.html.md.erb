---
layout: markdown_page
title: "Product Vision - Plan"
---

- TOC
{:toc}

## Overview 

- [Timeline-based roadmap view of upcoming planned improvements](https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Aplan&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS)
- [Upcoming planned strategic direction improvements](https://gitlab.com/groups/gitlab-org/-/boards/706864?label_name[]=direction) 
and [upcoming planned improvements in detail](https://gitlab.com/groups/gitlab-org/-/boards/706864)
for the next few milestones (monthly releases)

The [GitLab DevOps lifecycle](/direction/#devops-stages) contains the Plan stage.
The product vision of Plan is to enable all people in any size organization to:

- **Innovate** ideas.
- Organize those ideas into transparent, **multi-level work breakdown plans** that cut
across departments.
- **Track the execution** of these plans, adjusting them along the way as needed.
- **Collaborate** with team members, internal and external stakeholders, and executive
sponsors, using the same set of single-source-of-truth artifacts in a single application.
- **Measure** the efficacy of the entire process, characterizing the flows with 
**value streams**, thus providing actionable insight for continuous improvement.

This product vision is achieved through several **product categories** collected 
into **groups**.

In the **Team Planning** group, we have the [**Project Management**](#project-management), 
[**Kanban Boards**](#kanban-boards), and [**Time Tracking**](#time-tracking) product
categories. These are focused on helping individual product development teams deliver 
tangible business value to their customers, by enabling them to ship software, and 
in particular, the most important priorities, at a high velocity, sustainably over
a long-period of time (avoiding burnout). Kanban Boards will increasingly play a
crucial role here, with teams congregating within one or several boards that serve
as a central location for collaboration, progress tracking within a sprint, and
even retrospective evaluation afterward. Project Management improvements will continue
to be the glue that ties issues and their many attribute objects together, including
labels, milestones, weights, and assignees. Time Tracking is a strategic area to
incorporate management of the most imporant resource of Team Planning, namely that
of individuals and their valuable time.

In the **Enterprise Planning** group, we have [**Agile Portfolio Management**](#agile-portfolio-management),
and [**Value Stream Management**](#value-stream-management). Agile Portfolio Management
builds on Team Planning by helping _multiple_ teams deliver a coherent experience
for customers, with potentially many different product and services. In particular,
a _portfolio_ of initiatives are managed at the director level, and even at the
executive level. Agile Portfolio Management will thus be focused on work breakdown
plans that support both top-down and bottoms-up planning, as well as help business
sponsors make crucial decisions of where to invest resources that have longer-term
business ramifications, using ROI analysis, resource planning, financial management,
and other tools. Value Stream Management will help teams in an organization measure
their end-to-end cycle time of delivering value to customers, and even aggregate
that metric across different teams, and ultimately the entire organization. This
metric should be reduced as a best practice, and the vision of Value Stream Management
is to exactly enable that, by helping teams create and manage their custom DevOps
workflows, and identify where their most significant process inefficiencies lie,
in order to invest resources into mitigating those problems first.

In the **Certify** group, we have [**Requirements Management**](requirements-management),
[**Quality Management**](quality-management), and [**Service Desk**](service-desk).
Requirements Management will help organizations with more rigorous requirements
planning to track and manage changes and traceability of their intended software
(and even hardware) capabilities. Quality Management will help organizations maintain
processes and artifacts to manage and track software quality. Service Desk will
continue to help the customers of GitLab customers to easily send in feedback, whether
it be bugs, performance problems, or general feature requests. In particular, Service
Desk will be improved to truly realize the vision of bringing customers, support
teams, and product devlopment teams all closer together within one tool, in order
to reduce DevOps end-to-end cycle times and create the right software.

See many interesting features coming in 2019, as of late December 2018.

<figure class="video_container">
<iframe src="https://www.youtube.com/embed/qC9H-BChiwQ" frameborder="0" allowfullscreen="true" width="640" height="360"> </iframe>
</figure>

<%= partial("direction/categories", :locals => { :stageKey => "plan" }) %>

## Additional areas

Plan is also concerned with the following areas.

### Governed workflows

Large enterprises, especially those in highly-regulated industries such as finance
and healthcare, rely on governance frameworks in their planning and execution, in
order to mitigate a variety of business risks.

GitLab will be improved with [customized workflows integrated into boards](https://gitlab.com/groups/gitlab-org/-/epics/424), 
supporting the specific governed workflow stages in a particular organization. Also,
GitLab will have [custom fields](https://gitlab.com/groups/gitlab-org/-/epics/235), 
enabling the level of oversight and standardization required in the given large company.

See [upcoming planned improvements for governed workflows](https://gitlab.com/groups/gitlab-org/-/epics?label_name[]=governance).

### Search

Teams leverage GitLab search to quickly search for relevant content, enabling
stronger intra-team and cross-team collaboration through discovery of all GitLab
data.

See [upcoming planned improvements for search](https://gitlab.com/groups/gitlab-org/-/epics?label_name[]=search).

### Jira integration

GitLab supports deep Jira integration, allowing teams to use Jira for issue
mangement, but still leveraging the benefits of GitLab source control and other
native GitLab features.

See [upcoming planned improvements for Jira integration](https://gitlab.com/groups/gitlab-org/-/epics?label_name[]=Jira).

## Metrics

These Looker dashboards are available to GitLab team members only. [Get access to Looker](https://gitlab.com/gitlab-com/access-requests/issues/new?issuable_template=New%20Access%20Request). 

- [Plan: Counts of instances of instances using features](https://gitlab.looker.com/dashboards/41)
- [Plan: Total object counts](https://gitlab.looker.com/dashboards/63)

## How we prioritize

We follow the [general prioritization process](/handbook/product/#prioritization)
of the Product Team. In particular, we consider reach, impact, confidence, and
effort to identify and plan changes in upcoming milestones (monthly iterations).

- See a high-level [timeline-based roadmap view of planned upcoming improvements](https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=Plan&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS).
- See [upcoming planned strategic direction improvements](https://gitlab.com/groups/gitlab-org/-/boards/706864?label_name[]=direction) 
and [upcoming planned improvements in detail](https://gitlab.com/groups/gitlab-org/-/boards/706864)
for the next few milestones (monthly releases).

## Contributions and feedback

We love community code contributions to GitLab. Read
[this guide](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/index.md)
to get started.

Please also participate in our [issue tracker](https://gitlab.com/gitlab-org/gitlab-ce).
You can file bugs, propose new features, suggest design improvements, or
continue a conversation on any one of these. Simply [open a new issue](https://gitlab.com/gitlab-org/gitlab-ce/issues/new)
or comment on [an existing one](https://gitlab.com/gitlab-org/gitlab-ce/issues).

You can also contact me (Victor) via the channels on [my profile page](https://gitlab.com/victorwu).

## Direction

<%= direction %>

<%= partial("direction/other", :locals => { :stage => "plan" }) %>
